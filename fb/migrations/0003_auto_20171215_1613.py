# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-15 16:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('fb', '0002_auto_20171215_1444'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fbuser',
            name='online',
            field=models.CharField(choices=[('ONLINE', 'ONLINE'), ('NOT ONLINE', 'OFFLINE')], max_length=10),
        ),
    ]
