# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import FbUser, FbPosts, UserPostActions, FbUserNewsfeed, FbGroup, FbEvent, FbPage

class FbUserAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'gender',
        'DOB',
        'place',
        # 'friends',
        'online'
    )
admin.site.register(FbUser, FbUserAdmin)


class FbPostsAdmin(admin.ModelAdmin):
    list_display = (
        'post',
        'post_time',
        'postedby_fbuser'
    )
admin.site.register(FbPosts, FbPostsAdmin)


class UserPostActionsAdmin(admin.ModelAdmin):
    list_display = (
    'user',
    'post',
    'action_datetime',
    'action'
    )
admin.site.register(UserPostActions, UserPostActionsAdmin)


# class FbUserNewsfeedAdmin(admin.ModelAdmin):
#     list_display = (
#         'fbposts',
#         'name_fbuser'
#     )
admin.site.register(FbUserNewsfeed)


# class FbGroupAdmin(admin.ModelAdmin):
#     list_display = (
#     'group_name',
#     'group_members',
#     'fbusers'
#     )
admin.site.register(FbGroup)


class FbEventAdmin(admin.ModelAdmin):
    list_display = (
        'event',
        'event_time',
        'fbuser'
    )
admin.site.register(FbEvent, FbEventAdmin)


class FbPageAdmin(admin.ModelAdmin):
    list_display = (
        'page',
        'page_time',
        'fbuser'
    )
admin.site.register(FbPage, FbPageAdmin)