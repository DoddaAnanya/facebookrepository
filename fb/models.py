# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class FbUser(models.Model):
    GENDER_CHOICES = [
        ('MALE', 'M'),
        ('FEMALE', 'F')
    ]
    ONLINE_CHOICES = [
        ('ONLINE','ONLINE'),
        ('NOT ONLINE','OFFLINE')
    ]
    name = models.CharField(max_length=1000, unique=True)
    gender = models.CharField(max_length=10, choices=GENDER_CHOICES)
    DOB = models.DateTimeField(null=False, blank=False)
    place = models.CharField(max_length=100, unique=True)
    friends = models.ManyToManyField('self',blank=True)
    online = models.CharField(max_length=10, choices=ONLINE_CHOICES)


class FbPosts(models.Model):
    post = models.CharField(max_length=1000)
    post_time = models.DateTimeField(null=True, blank=True)
    postedby_fbuser = models.ForeignKey(FbUser)


class UserPostActions(models.Model):
    ACTION_CHOICES = [
        ('LIKE', 'LIKE')
    ]
    user = models.ForeignKey(FbUser)
    post = models.ForeignKey(FbPosts)
    action_datetime = models.DateTimeField()
    action = models.CharField(max_length=100, choices=ACTION_CHOICES)


class FbUserNewsfeed(models.Model):
    fbposts = models.ManyToManyField(FbPosts)
    name_fbuser = models.ForeignKey(FbUser)


class FbGroup(models.Model):
    group_name = models.CharField(max_length=100)
    group_members = models.ManyToManyField(FbUser, related_name='groups')
    fbuser = models.ForeignKey(FbUser)


class FbEvent(models.Model):
    PLACE_CHOICES = [
        ('HYDERABAD', 'HYD'),
        ('VIJAYAWADA', 'VIJ'),
        ('TIRUPATHI', 'TPT'),
        ('VIZAG', 'VIZ')
    ]
    event = models.CharField(max_length=100, choices=PLACE_CHOICES)
    event_time = models.DateTimeField('date of event')
    fbuser = models.ForeignKey(FbUser)


class FbPage(models.Model):
    page = models.CharField(max_length=100)
    page_time = models.DateTimeField('date of page created')
    fbuser = models.ForeignKey(FbUser)